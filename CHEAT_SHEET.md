<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Cheat Sheet: GNSS DB Interaction](#cheat-sheet-gnss-db-interaction)
  - [Installation](#installation)
    - [Required software:](#required-software)
      - [Python](#python)
      - [GitLab repos](#gitlab-repos)
      - [Anubis](#anubis)
      - [CRX2RNX](#crx2rnx)
      - [RunQC](#runqc)
    - [Optional software:](#optional-software)
      - [Postman](#postman)
      - [Swagger](#swagger)
      - [DBeaver](#dbeaver)
  - [DB, server setup](#db-server-setup)
  - [Indexing data](#indexing-data)
  - [Web Service Testing](#web-service-testing)
  - [Web Service Documentation](#web-service-documentation)
  - [Version Control](#version-control)
  - [Bugs, Limitations](#bugs-limitations)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Cheat Sheet: GNSS DB Interaction
Author: Tim Sonnemann (created: 2018-01-25, last mod: 2018-02-19)

This document gives a quick run-down of all typical commands to test the Flask web server's functionality. It is assumed that there is a folder `gpseurope` and at least the following repositories are in it: `database`, `fwss`, `indexGD` and `RunQC`.

Further, all commands should be executed in Linux shells (tested on Ubuntu 16.04), and the latest __master branch__ should be used for testing.

More detailed descriptions and notes can be found in each repo's README.md files. If anything is wrong or missing here, please post an Issue.

## Installation

To configure git the first time [see here](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html).

### Required software:

#### Python
When using `pip` to get Python packages, make sure it's Python 2 (the code has not been ported to Python 3). Using `pip2` instead of `pip` may be necessary depending on your configuration. Check with: `pip --version`

```sh
sudo apt-get install git python python-pip postgresql
sudo pip install pip -U
sudo pip install flask sqlalchemy psycopg2 requests xmltodict flask_cors
```

#### GitLab repos
```sh
cd $HOME/my/git/projects  # decide where to put the repos
mkdir gpseurope
cd gpseurope
git init
git clone git@gitlab.com:gpseurope/database.git
git clone git@gitlab.com:gpseurope/fwss.git
git clone git@gitlab.com:gpseurope/indexGD.git
git clone git@gitlab.com:gpseurope/RunQC.git
```

#### [Anubis](http://www.pecny.cz/Joomla25/index.php/gnss/sw/anubis)
* check latest available version [here](http://www.pecny.cz/sw/anubis/)

```sh
sudo apt-get install automake zlib1g-dev
cd ~/progs  # decide where to put the program
wget 'http://www.pecny.cz/sw/anubis/anubis-2.1.3-2018-02-05.tgz'
tar -xzf anubis-2.1.3-2018-02-05.tgz 
cd anubis
./autogen.sh
ln -s ~/progs/anubis/app/anubis ~/bin/anubis
```

#### [CRX2RNX](http://terras.gsi.go.jp/ja/crx2rnx.html)
* check above link for available package types

```sh
cd ~/progs  # decide where to put the program
wget 'http://terras.gsi.go.jp/ja/crx2rnx/RNXCMP_4.0.7_Linux_x86_64bit.tar.gz'
tar -xzf RNXCMP_4.0.7_Linux_x86_64bit.tar.gz
ln -s ~/progs/RNXCMP_4.0.7_Linux_x86_64bit/bin/CRX2RNX ~/bin/CRX2RNX
```

#### RunQC
* to use `RunQC.pl`, need Perl module `XML::LibXML`

```sh
cpan XML::LibXML
```
Or if using `cpan` the first time:

```sh
cpan  # agree to automatic configuration
cpan[1]> install XML::LibXML
cpan[2]> q
```

* make available in `$PATH`

```sh
ln -s $HOME/my/git/projects/gpseurope/RunQC/RunQC.pl ~/bin/RunQC
```

### Optional software:

#### [Postman](https://www.getpostman.com/)
Convenient GUI for web API testing. *Free* basics, *commercial* extras.
* manual install:
    - choose and download the package from the website
    - unpack it to a chosen location
    - the executable `Postman` can be used directly
* command line installation: (assuming `~/prog` as destination)

```sh
wget https://dl.pstmn.io/download/latest/linux64 -O postman.tar.gz
tar -xzf postman.tar.gz -C ~/prog
rm postman.tar.gz
ln -s ~/prog/Postman/Postman ~/bin/postman
```

* create menu launcher: (adapt icon path!)

```sh
cat <<EOL> .local/share/applications/postman.desktop
[Desktop Entry]
Encoding=UTF-8
Name=Postman
Exec=postman
Icon=$HOME/prog/Postman/resources/app/assets/icon.png
Terminal=false
Type=Application
Categories=Development;
```

#### [Swagger](https://swagger.io/)
Convenient GUI for web API description and testing. *Free* basics, *commercial* extras.
* installation instructions also [here](https://swagger.io/docs/swagger-tools/#download-33), basically this:

```
cd my_main_git_directory # adapt at will
git clone https://github.com/swagger-api/swagger-ui.git
```

* usage: open `swagger-ui/dist/index.html` in a browser
    - the swagger search bar should point to the wanted YAML or JSON (see below)

#### [DBeaver](https://dbeaver.jkiss.org/)
Convenient GUI for database access. *FOSS*, *commercial* extras.

```sh
cd $HOME/Downloads
wget 'https://dbeaver.jkiss.org/files/dbeaver-ce_latest_amd64.deb'
sudo apt install dbeaver-ce_{version number}_amd64.deb
```

## DB, server setup

* Store your working directory for convenience, e.g.

```sh
WP10=$HOME/my/git/projects/gpseurope
```

* setup new database (drop existing if wanted)
    - assuming you have created a postgres role (user) already
    - check whether overwriting existing DB of same version, comment out accordingly

```sh
cd $WP10/database
role="fwss"
db="gnss-europe-v0-2-13"
fsql="gnss-europe.pgdump.sql"
# if replacing DB of same name/version:
dropdb -U $role -h 127.0.0.1 -i -e $db
# if no such DB exists now:
createdb -U $role -h 127.0.0.1 $db
# connect to $db and execute $fsql script:
psql -U $role -h 127.0.0.1 -d $db < $fsql
```

* start web server
    - first ensure that `web_server.cfg` is up to date (DB version)

```sh
cd $WP10/fwss
# cp web_server.cfg.default web_server.cfg
python web_server.py
```

## Indexing data
Using the scripts in the `fwss` repository.
```sh
cd $WP10/fwss
```

* index sitelog JSON
    - first adapt default script to use some input directory

```sh
# cp import_sitelogs.py.default import_sitelogs.py
python import_sitelogs.py
```

* index datacenter
    - requires corresponding agency to be in DB already (done via sitelog...)
    - this can add a new file type as well (e.g. RINEX2, 24h, 15s not pre-filled)

```sh
python import_datacenters.py
```

* index RINEX files
    - more realistic example would use option for relative path `-r <pattern>`

```sh
python ../indexGD/indexGeodeticData.py \
         -p examples/RINEX \
         -w http://localhost:5000/gps/data/rinex \
         -o examples/rinex_file.json \
         -t RINEX2 -s 24h -f 15s -d IMO
```

* run Anubis and index QC info
    - may be replaced by only using `RunQC.pl` in future (?) versions

```sh
python import_qcxml.py
```

## Web Service Testing
* start Postman
    - create account to have online sync + storage
    - select `File > Import...` and import the JSON from `$WP10/fwss/Postman/`
    - testing functionality for the GNSS server's GET and POST endpoints should be available now

## Web Service Documentation
* Swagger
    - edit `swagger-ui/dist/index.html` (vim, gedit...)
    - put `url: "http://localhost:5000/gps/geodetic.yaml",` instead of the default url (in bottom quarter of file)
    - can also paste that url into the swagger search bar in browser instead
    - either double-click the `index.html` or open by CL

```sh
cd my_main_git_directory/swagger/swagger-ui/dist
firefox index.html
```

## Version Control
* GitLab

## Bugs, Limitations

* datacenter indexing: if agency not found, still responds as success
    - given id is then a tuple(msg-str, code)
* import_sitelogs.py: sitelog dir should be CLI, not static in code
* same for import_datacenters.py
* same for import_qcxml.py
* indexGD: no check for path existence
* ...
* see Issues of each GitLab repo