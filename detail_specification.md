<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  

- [1. Introduction](#1-introduction)
- [2. QC Insert/update](#2-qc-insertupdate)
  - [2.1 Rinex status evaluation](#21-rinex-status-evaluation)
  - [2.2 Status evaluation](#22-status-evaluation)
  - [2.3 validate QC data against DB (T1-T2) or fixed values](#22-validate-qc-data-against-db-t1-t2-or-fixed-values)
  - [2.4 Other validation on QC data](#22-other-validation-on-qc-data)
    - [2.4.1 MISSING OBSERVATION DATA](#221-missing-observation-data)
    - [2.4.2 MISSING OBSERVATION DATA](#222-missing-observation-data)
    - [2.4.3 LARGE SPP HEAD-SYS DIFFERENCE](#223-large-spp-head-sys-difference)
    - [2.4.4 LARGE SPP DEVIATION](#224-large-spp-deviation)
    - [2.4.5 MISSING CONSTELLATION SPP](#225-missing-constellation-spp)
    - [2.4.6 FEW SATELLITES IN DATA](#226-few-satellites-in-data)
    - [2.4.7 MORE THAN 50% OF UNUSABLE EPOCHS FOR CODE OBS](#227-more-than-50%25-of-unusable-epochs-for-code-obs)
    - [2.4.8 MORE THAN 50% OF UNUSABLE EPOCHS FOR PHASE OBS](#228-more-than-50%25-of-unusable-epochs-for-phase-obs)
    - [2.4.9 MISSING CONSTELLATION DATA](#229-missing-constellation-data)
    - [2.4.10 LOW NAVIGATION DATA](#2210-low-navigation-data)
    - [2.4.11 MISSING NAVIGATION DATA](#2211-missing-navigation-data)
    - [2.4.12 CONSTELLATION NOT LISTED AS RECEIVER SAT SYS](#2212-constellation-not-listed-as-receiver-sat-sys)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## 1. Introduction

This  document describes in detail some  of a Web Services Server functionalities. 

**Author:** Sergio Bruni (sergio.bruni@ingv.it)

**Document created:** 11. March 2019

**Last updated:** 12. March 2019

## 2. QC Insert/update ##

### 2.1 Rinex file status

Rinex status assume the following values:

- **[-3]** ERROR due to severe T3 - T1/T2 mismatch
- **[ 2]** WARNING due to marginal T3 - T1/T2 mismatch
- **[ 3]** WARNING due to minimum QC metrics on data quality
- **[ 1]** OK, check successfull and no significant issue identified
- **[ 0]** not checked yet

and two values reserved:
- **[-1]** QC running 
- **[-2]** QC could not be completed

### 2.2 Status priority settings

At the start of processing, if previous QC data are already present for the rinex file, they are deleted and the status is set to initial value of **0**

The QC data provided  are subjected to various checks executed sequentially, like the followings:

- compare with T1/T2 data stored in DB
- compared with  fixed values, range, threshold etc...
- cross congruence check 

Each check is associated with a failure status, between 2, 3 and -3.

While a check fail the status is changed if the failure status of the check is more severe than the current:

- from 0, 2 or 3 to -3
- from 0 or 2 to 3
- from 0 to 2 

If all checks succeed  the status is set to 1


### 2.3 Validate QC data against DB (T1-T2) or fixed values

| Parameter     | QC (anubis XML)                                              | Expected                         | check | Tolerance | status | label | text |
| ------------- | ------------------------------------------------------------ | -------------------------------- | ----- | --------- | ------ | ----- | ---- |
| file_format   | head&zigrarr;file_format                                     | DB_T2                            | EQ    | -         | -3     | ERROR | PDNM |
| file_name     | head&zigrarr;file_name                                       | DB_T2                            | EQ    | -         | 2      | ERROR | PDNM |
| antenna_type  | head&zigrarr;antenna_type                                    | DB_T1                            | EQ    | -         | -3     | ERROR | PDNM |
| receiver_type | head&zigrarr;receiver_type                                   | DB_T1                            | EQ    | -         | -3     | ERROR | PDNM |
| antenna_dome  | head&zigrarr;antenna_dome                                    | DB_T1                            | EQ    | -         | -3     | ERROR | PDNM |
| antenna_numb  | head&zigrarr;antenna_numb                                    | DB_T1                            | EQ    | -         | 2      | WARN  | PDNM |
| receiver_numb | head&zigrarr;receiver_numb                                   | DB_T1                            | EQ    | -         | 2      | WARN  | PDNM |
| site_id       | head&zigrarr;site_id                                         | DB_T1                            | EQ    | -         | 2      | WARN  | PDNM |
| marker_numb   | head&zigrarr;marker_numb                                     | DB_T1                            | EQ    | -         | 2      | WARN  | PDNM |
| ecc_n/e/u     | head&zigrarr;position &zigrarr;eccentricity &zigrarr;axis &zigrarr;name &zigrarr;N/E/U | DB_T1                            | EQ    | 0.0001    | -3     | ERROR | PDTL |
| x/y/z_crd     | head&zigrarr;position &zigrarr;coordinate &zigrarr;gml:Point &zigrarr;gml:pos[0/1/2] | DB_T1                            | EQ    | 100       | 2      | WARN  | PDTL |
| data_smp      | data&zigrarr;data_int                                        | DB_T2                            | EQ    | 0.001     | 2      | WARN  | DSI1 |
| data_int      | head&zigrarr;data_int                                        | anubis_XML data&zigrarr;data_int | EQ    | 0.001     | 2      | WARN  | DSI2 |
| data_len      | data&zigrarr;date_end - data&zigrarr;date_beg                | DB_T2 (50%)                      | GE    | 0         | 2      | WARN  | DLI1 |
| data_len2     | data&zigrarr;data_int * data&zigrarr;numb_epo                | DB_T2 (50%)                      | GE    | 0         | 2      | WARN  | DLI1 |
| elev_cutoff   | meta&zigrarr;settings &zigrarr;elev_min                      | DB_T1                            | GE    | 2.5       | 2      | WARN  | PDTL |
| xint          | data&zigrarr;excluded &zigrarr;xint                          | 0                                | EQ    | 0         | 3      | WARN  | DINV |
| xbeg          | data&zigrarr;excluded &zigrarr;xbeg                          | 0                                | EQ    | 0         | 3      | WARN  | DINV |
| xend          | data&zigrarr;excluded &zigrarr;xend                          | 0                                | EQ    | 0         | 3      | WARN  | DINV |
| xsys          | data&zigrarr;excluded &zigrarr;xsys                          | 0                                | EQ    | 0         | 3      | WARN  | DINV |
| clk_jmps      | data&zigrarr;total &zigrarr;clk_jmps                         | 0                                | EQ    | 0         | 3      | WARN  | DINV |
| numb_epo      | data&zigrarr;numb_epo                                        | 60                               | GE    | 0         | 3      | WARN  | DCCI |
| numb_gap      | data&zigrarr;numb_gap                                        | 3                                | LE    | 0         | 3      | WARN  | DCCI |
| cyc_slps      | data&zigrarr;total &zigrarr;cyc_slps                         | 1500                             | LE    | 0         | 3      | WARN  | DCCI |
| obs_elev      | data&zigrarr;total &zigrarr;elev_min                         | 10                               | LE    | 3         | 3      | WARN  | DCCI |
| ratio         | data&zigrarr;total &zigrarr;have / data&zigrarr;total &zigrarr;expt | 0.4                              | GE    | 0         | 3      | WARN  | DOHE |



**Text Messages**

| Code | Text                                             |
| ---- | ------------------------------------------------ |
| PDNM | PARAMETERS DO NOT MATCH                          |
| PDTL | PARAMETER DIFFERENCE TOO LARGE                   |
| DINV | DATA ISSUE - NONZERO VALUE                       |
| DCCI | DATA CONSISTENCY/COMPLETENESS ISSUE              |
| DOHE | DATA OBS HAVE/EXPT RATIO < 0.4                   |
| DSI1 | DATA SAMPLING ISSUE: QC DATA VS T2 MISMATCH      |
| DSI2 | DATA SAMPLING ISSUE: QC HEAD VS QC DATA MISMATCH |
| DLI1 | DATA LENGTH ISSUE: QC BEG-END VS T2 MISMATCH     |



### 2.4 Other validation on QC data (anubis XML)

#### 2.4.1 MISSING OBSERVATION DATA

For every  ***head &zigrarr; system &zigrarr; obs*** do not exist a  ***data &zigrarr; system &zigrarr; obs*** of the same type.

On check failure status_code is set to 2.

The warning message is like the following (with system=GPS and obs type=C2W):

*WARNING (QC VALIDATION): MISSING OBSERVATION DATA (GPS): C2W; STATUS: 2*

#### 2.4.2 MISSING CONSTELLATION SPP

For a constellation defined in  **head &zigrarr; system**  is not defined the position in **data &zigrarr; position**

this check is restricted to the following systems: GPS, GLO, BDS, GAL.

On check failure status_code is set to 2, only if system is GPS 

The warning message is like the following (with system=GPS):

*WARNING (QC VALIDATION): MISSING CONSTELLATION SPP: GPS ; STATUS: 2* 

#### 2.2.3 LARGE SPP HEAD-SYS DIFFERENCE

#### 2.2.4 LARGE SPP DEVIATION

#### 2.4.5 FEW SATELLITES IN DATA

#### 2.4.6 MORE THAN 50% OF UNUSABLE EPOCHS FOR CODE OBS

#### 2.4.7 MORE THAN 50% OF UNUSABLE EPOCHS FOR PHASE OBS

#### 2.4.8 MISSING CONSTELLATION DATA

#### 2.4.9 LOW NAVIGATION DATA

#### 2.4.10 MISSING NAVIGATION DATA

#### 2.4.11 CONSTELLATION NOT LISTED AS RECEIVER SAT SYS

#### 2.2.12 MULTIPLE CONTEMPORARY ITEM/ATTRIBUTE







