<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Use Cases for Data Indexing and Quality Check](#use-cases-for-data-indexing-and-quality-check)
  - [1.  Use Cases](#1--use-cases)
    - [1.1  Manual mode](#11--manual-mode)
    - [1.2  Archive mode](#12--archive-mode)
    - [1.3  Operating mode (scheduler or trigger)](#13--operating-mode-scheduler-or-trigger)
    - [1.4  Bucket mode](#14--bucket-mode)
    - [1.5  Hourly mode](#15--hourly-mode)
    - [1.6  QC-only mode](#16--qc-only-mode)
  - [2.  Tools](#2--tools)
    - [2.1  IndexGD](#21--indexgd)
      - [2.1.1 Usage](#211-usage)
      - [2.1.2 Output](#212-output)
    - [2.2  RunQC](#22--runqc)
      - [2.2.1 Usage](#221-usage)
      - [2.2.2 Output](#222-output)
    - [2.3 rQC2DB](#23-rqc2db)
      - [2.3.1 Usage](#231-usage)
    - [2.4 QC2DB](#24-qc2db)
      - [2.4.1 Usage](#241-usage)
    - [2.5  DB-API](#25--db-api)
      - [2.5.1 Usage](#251-usage)
        - [2.5.1.1 POST `data/rinex`](#2511-post-datarinex)
        - [2.5.1.2 GET `data/rinex/{marker}`](#2512-get-datarinexmarker)
        - [2.5.1.3 POST `data/qcfile`](#2513-post-dataqcfile)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

---
title: "EPOS WP10 (GNSS)"
subtitle: "Use Cases for Data Indexing and Quality Check"
author: [Tim Sonnemann, Jan Dousa]
date: 2018-03-06
tags: [Markdown, EPOS, GNSS, Use Cases, WP10]
titlepage: true
colorlinks: true
...

# Use Cases for Data Indexing and Quality Check

|  |  |
| --- | --- |
| Version: | 1.2.3 |
| Dated:   | 2018-03-06 |
| Authors: | Jan Dousa (jan.dousa@pecny.cz), <br/>Tim Sonnemann (tim@vedur.is) |

**Purpose of this document**

Here, the [EPOS](https://www.epos-ip.org/) [WP10](https://www.epos-ip.org/tcs/gnss-data-and-products) **GNSS** [database](https://gitlab.com/gpseurope/database) (**DB**) metadata (**MD**) **indexing** of RINEX files and their quality checking (**QC**) is described. The available software tools, their modes of operation, their respective input and output are listed and described in detail.


## 1.  Use Cases

All standard tools (**IndexGD, RunQC, DB-API**) should support
different operating modes via a flexible tool configuration. The
**INSERT** vs. **UPDATE** functionality is not distinguished, but is
assumed to be implicitly resolved by the tools. An overview of available
tools (core and wrappers) is given in Table 1.

**Table 1:** Existing tools to extract T2/3 metadata and
index them to the GNSS database. The bold input parameters
are mandatory. The bold output info highlights possible types of
output (e.g. file) or action achieved (e.g. post, i.e. upload).

| **Tool** | **Input** | **Output** |
| --- | --- | --- |
|**IndexGD** | **path\_to\_dir OR file\_list, output\_type**, file\_info (type, frequency, length), data\_center, target\_url\_pattern | T2 md (JSON) **file** and/or **posted** to DB-API |
|**RunQC**  | **path\_to\_dir\_pattern + start\_time OR input\_JSON** | T3 md (QC-XML) **file**(s) |
|**QC2DB**  | **list-of-QC-XML** (*NOT IMPLEMENTED, SEE DETAILS*)     | T3 md files **posted** to DB-API |
|**rQC2DB**| (*HARD-CODED DEV SCRIPT*)                               | T3 md (QC-XML) **file**(s) **AND post** to DB-API |
|**DB-API** | `POST` **T2(JSON)**                                       | **Index** T2 md in DB, **set** T2 file status |
|**DB-API** | `GET` **parameter: marker** [,start/end date, status]     | **Return** T2 md from DB |
|**DB-API** | `POST` **T3(QC-XML)**                                     | **Index** T3 md in DB, **validate** T1/2/3 md, **update** T2 file status |


The envisioned operating modes are listed and described in the following sections.

### 1.1  Manual mode
All tools started manually and independently on a batch of files:

1.  Run `IndexGD` with given list/dir + type_info
    - **T2 md** *created* and *indexed* to **DB** via **DB-API**.
2.  Run `rQC2DB` with same basic input
    - **T3 md** *created* by `RunQC` and *indexed* to **DB** + **T123val** via **DB-API**.


Or even more piecemeal approach:

1. Run `IndexGD` with given list/dir + type_info
    - **T2 md** *created* and *indexed* to **DB** via **DB-API**.
2. Run `RunQC` with given file mask
    - **T3 md** *created* (files).
3. Run `QC2DB` with given QC-file mask
    - **T3 md** *indexed* to **DB** + **T123val** via **DB-API**.

*NOTE:* This can be updated in the future, as `RunQC` may receive the ability to `post` its QC-XML directly to the DB-API.

### 1.2  Archive mode
Index and QC files from historical archive (easy support for new GLASS nodes)

* script wrapper over **Manual mode**
* script wrapper over **Bucket mode** (optionally)
* usable later also for the file update

### 1.3  Operating mode (scheduler or trigger)
Index incoming files (recent):

1.  Run `IndexGD` with given list/dir (new files) + type_info.
2.  Run `RunQC` with instruction to ask **DB-API** for *status=0* files.
    - **DB-API** *returns* **T2 md** JSON as input to `RunQC` (*TODO!*)
    - `RunQC` *creates* **T3 md** QC-XML
    - `RunQC` *posts* **T3 md** QC-XML to **DB-API** (*TODO!*): *index* to **DB** + **T123val**


* **Q.:** What is the preferable way of operation:


1.  `IndexGD` and `RunQC` independently ?
2.  `IndexGD` and `RunQC` combined ?


* Should `RunQC` include communication with **DB-API**?
    - If yes, `RunQC` needs to be configured accordingly (but does it work already?)
    - If not, an additional tool such as `QC2DB` could handle *posting* **T3 md**, but there is nothing yet to *query* **T2 md** and hand it to `RunQC`!

*NOTE:* This can be updated in the future, as `RunQC` may receive the ability to *query* the **DB-API**, receive and understand **T2 md** JSON, and *post* its QC-XML directly to the **DB-API**.

### 1.4  Bucket mode
Sequential data indexing and quality check (both actions combined)

* can be implemented by script wrapper over **Manual mode** or 
**Operating mode**

* can implement additional functionality, such as file manipulation
etc..

### 1.5  Hourly mode
Index fast hourly files with a minimum QC (i.e. without navigation data ?)

### 1.6  QC-only mode
Only the quality control (e.g. update of the QC)

* script wrapper over **Manual mode**

## 2.  Tools

The software tools developed for generating DB input and interacting with the DB are briefly described in the following sections. For detailed descriptions and source code, follow the links to their respective GitLab repos.

### 2.1  IndexGD

*Index geodetic data*

The main script for [IndexGD](https://gitlab.com/gpseurope/indexGD) is [`indexGeodeticData.py`](https://gitlab.com/gpseurope/indexGD/blob/master/indexGeodeticData.py).

#### 2.1.1 Usage

```bash
python indexGeodeticData.py [-h] [-p PATH] [-w WEB_SERVICE_PATH]
                            [-t FILE_TYPE] [-s SAMPLING_WINDOW]
                            [-f SAMPLING_FREQUENCY] [-d DATA_CENTER]
                            [-m FILE_MASK] [-l LIST_FILES] [-o OUTPUT_FILE]
                            [-r RELATIVE_PATH]
```

#### 2.1.2 Output

`IndexGD` generates a JSON structure containing RINEX file metadata which is posted to DB-API for every RINEX file to be indexed. The designated web service endpoint is `http//<host>[:<port>]/gps/data/rinex`. See the model with example values below:

```json
{
    "data_center": "IMO",
    "file_name": "RHOF0240.17D.Z",
    "file_size": 825803,
    "file_type": "RINEX2",
    "md5_checksum": "acc7f446cfa287975e28ef11c81ec892",
    "md5uncompressed": "9db2e9777d4c4ddb058bde785f7f8c56",
    "published_date": "2018-01-02",
    "reference_date": "2017-01-24",
    "relative_path": "2017/024",
    "sampling_frequency": "15s",
    "sampling_window": "24h",
    "station_marker": "RHOF"
}
```

### 2.2  RunQC

*Run quality control*

The main script for [RunQC](https://gitlab.com/gpseurope/RunQC) is [`RunQC.pl`](https://gitlab.com/gpseurope/RunQC/blob/master/RunQC.pl). `RunQC.pl` is a wrapper for the [`G-Nut/Anubis`](http://www.pecny.cz/Joomla25/index.php/gnss/sw/anubis) software. `RunQC` will download navigation files, uncompress input RINEX files, configure and run `Anubis` on the input files. `Anubis` writes a QC metadata file known as `QC-XML`, which contains general metadata, RINEX header info and QC summary data. This `QC-XML` can be posted directly to the DB-API, which will parse and index it. `RunQC` is expected to add DB-API querying and posting features in the future.

#### 2.2.1 Usage

```bash
RunQC.pl
      --ref_date string  # reference time (=start time) for data batch ("YYYY-MM-DD HH:MM:SS")
      --fil_mask string  # local mask to files in repository
      --dir_brdc string  # local path to brdc local archive
      --upd_brdc integer # update brdc files in local archive [default:1]
      --inp_json string  # input JSON file
      --out_json string  # store JSON file
      --dir_out  string  # output directory (can include gps_date format)
      --verb     integer # level of verbosity
      --debug            # debug
      --help             # help message
```

#### 2.2.2 Output

The output of `RunQC` is a `QC-XML` file for each processed RINEX file. It is stored locally into a designated directory, although directly posting it to the DB-API may be implemented in the future. Instead, another script is used during development to both execute `RunQC` and upload each resulting `QC-XML` to the web service endpoint `http//<host>[:<port>]/gps/data/qcfile`.


### 2.3 rQC2DB

*run quality control and put it to database*

The main script for [rQC2DB](https://gitlab.com/gpseurope/fwss) is [`import_qcxml.py`](https://gitlab.com/gpseurope/fwss/blob/master/import_qcxml.py). This wrapper script executes `RunQC` (with hard-coded settings) and posts the resulting QC-XML files to the DB-API. A separate script to only post existing files (here: `QC2DB`) has not been created as of now (2018-02-22). This script so far is only for development purposes.

#### 2.3.1 Usage

Everything is hard-coded, as it is just a basic testing script for development.
```bash
python import_qcxml.py
```

The script expects to find RINEX files, passes the information to `RunQC` and executes it, locates the resulting `QC-XML` file and posts it to the web service endpoint `http//<host>[:<port>]/gps/data/qcfile`.

### 2.4 QC2DB

*put quality control data to database*

There is no such script yet, although it may be simple to write. The existing tool `rQC2DB` could be altered to contain only the QC-XML file *searching* and *posting* functionalities.

#### 2.4.1 Usage

Presumably something like:
```bash
python import_qcxml.py -p PATH -w WEB_SERVICE_PATH [-l FILE_LIST]
```

The script should find QC-XML files, either by path pattern or from a list in a text file, and post them to the web service endpoint `http//<host>[:<port>]/gps/data/qcfile`.

### 2.5  DB-API

The main scripts for [DB-API](https://gitlab.com/gpseurope/fwss) are [`web_server.py`](https://gitlab.com/gpseurope/fwss/blob/master/web_server.py) and [`queries.py`](https://gitlab.com/gpseurope/fwss/blob/master/queries.py). The `web_server.py` script uses the [Flask](http://flask.pocoo.org/) web framework to offer a RESTful web service, while it calls on `queries.py` to execute its actual services by parsing input, querying the DB and returning postprocessed responses.

#### 2.5.1 Usage

This describes the web service usage, not the setup and configuration of the server software.

##### 2.5.1.1 POST `data/rinex`

This endpoint receives a RINEX file metadata JSON text body and indexes the information into the DB.

* **Parameters:** 
    - **body** (*body*), required, RINEX (T2) metadata (*JSON*)

* **Example**

```json
{
    "data_center": "IMO",
    "file_name": "RHOF0240.17D.Z",
    "file_size": 825803,
    "file_type": "RINEX2",
    "md5_checksum": "acc7f446cfa287975e28ef11c81ec892",
    "md5uncompressed": "9db2e9777d4c4ddb058bde785f7f8c56",
    "published_date": "2018-01-02",
    "reference_date": "2017-01-24",
    "relative_path": "2017/024",
    "sampling_frequency": "15s",
    "sampling_window": "24h",
    "station_marker": "RHOF"
}
```

##### 2.5.1.2 GET `data/rinex/{marker}`

This endpoint returns a JSON array of indexed RINEX file metadata for a particular station. The station marker is a mandatory path parameter, and there are several optional query parameters.

* **Parameters** 
    - **marker** (*path*), required, station short marker (*string*)
    - **date_from** (*query*), optional, start reference time (*string/dateformat*)
    - **date_to** (*query*), optional, end reference time (*string/dateformat*)
    - **status** (*query*), optional, file status (*integer*)

**Responses**

If successful, a JSON array of RINEX file metadata will be sent back. It contains additional information to the T2 input: `station_name`,  `protocol`, `root_path`, `directory_naming`, `creation_date`, `status`. 

```json
[
  {
    "id": 0,
    "station_marker": "string",
    "station_name": "string",
    "file_name": "string",
    "file_type": "string",
    "sampling_window": "string",
    "sampling_frequency": "string",
    "data_center": "string",
    "protocol": "string",
    "hostname": "string",
    "root_path": "string",
    "directory_naming": "string",
    "relative_path": "string",
    "reference_date": "string",
    "creation_date": "string",
    "published_date": "string",
    "file_size": 0,
    "md5_checksum": "string",
    "md5_uncompressed": "string",
    "status": 0
  }
]
```

##### 2.5.1.3 POST `data/qcfile`

This endpoint receives a QC-XML as text body, parses its contents, validates the T1-2-3 metadata by comparing them, and indexes the QC-info and a resulting RINEX file status code to the DB.

* **Parameters:** 
    - **body** (*body*), required, QC-XML (T3) metadata (*XML*)
