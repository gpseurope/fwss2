#!/bin/bash

# with titlepage (in document's "header" metadata),
# with table of contents (--toc option),
# and code blocks shown by LaTeX "listings" option,
# using xelatex engine as pdflatex cannot handle unicode:
pandoc T23_UseCases.md \
  -o T23_UseCases.pdf \
  --from markdown \
  --template eisvogel_template.latex \
  --listings \
  --pdf-engine=xelatex \
  --toc

# LaTeX Template:
# Get the Eisvogel template for pandoc-latex from Wandmalfarbe's GitHub:
# https://github.com/Wandmalfarbe/pandoc-latex-template
# That template is under a 3-clause BSD license copyrighted by
# John MacFarlane and Pascal Wagler.
