<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  

- [1. Introduction](#1-introduction)
- [2. index rinex](#2-index-rinex)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## 1. Introduction

This  document describes in detail the process of index or update a rinex file on database by the Web Services Server. 

**Author:** Sergio Bruni (sergio.bruni@ingv.it)

**Document created:** 12. November 2019

**Last updated:** 13. November 2019

## 2. index rinex  ##

[rinex_update_scenario](https://app.diagrams.net/#G1-XS3u1hvX67bSGsTu5yEwMw2CQCFQALV)


Part of index rinex flow process, to establish if it is a new rinex or an existing one's:

![](rinex_update_scenario.jpg)



