This directory holds some example RINEX data files for station RHOF.

They can be indexed to the database via Flask server endpoint:
```
http//localhost:5000/gps/data/rinex
```

This can be done using the script `indexGeodeticData.py` in repo `indexGD`.
The file type cannot be reliably understood yet by that script (as of 2018-01-22), so options must be given as well. The example files are RINEX2, 24h, 15s from data center IMO.

The corresponding command would be:
```
python indexGeodeticData.py \
         -p examples/RINEX \
         -w http://localhost:5000/gps/data/rinex \
         -o test_rinex.json \
         -t RINEX2 -s 24hour -f 15s -d IMO
```
It also produces a file `test_rinex.json` which contains all JSON blocks to be posted to the webserver.