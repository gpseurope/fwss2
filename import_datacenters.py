import glob, os
import requests
import json

# This points to the directory where your JSON datacenters reside. 
os.chdir("examples_datacenter")

# The server is set default as 'localhost'.
# Modify if needed.
server_ip_address = 'localhost'


# Loop through the folder and process each sitelog
for fname in glob.glob("*.json"):
    file = open(fname, 'r') 
    body = file.read()

    address = 'http://'+server_ip_address+':5000/gps/datacenter'

    r = requests.post(address, data=body)
    print "File posted {} Status code: {}".format(
        fname, r.status_code) #, r.text.encode('utf-8')
    # we won't get JSON if the query script raises an error
    try:
        p_json = json.loads(r.text) # parse JSON string
        print json.dumps(p_json, indent=4, sort_keys=True)
    except ValueError:
        print r.text
    except:
        print 'Unexpected error!'
        raise
