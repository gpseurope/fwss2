import glob, os
import json
import requests
import subprocess
from datetime import datetime

"""
This script finds RINEX files, applies RunQC to them and pushes the resulting
QC-XML files to the webserver.
By default this is set to the examples folder which contains a RINEX dir,
and a LOG dir will be created here by RunQC.
"""

# Enter the dir in which RunQC.pl is supposed to run and create a LOG dir:
wdir = 'examples'
# Enter the dir with RINEX files to be indexed: (no single file for now)
# This is either relative from 'wdir', or give an absolute path.
dir_rinex = 'RINEX'

# RunQC executable to use, relative from wdir:
rqc = '../../RunQC/RunQC.pl --json_out qc_out_list.json'
#
os.chdir(wdir)

# The server is set default as 'localhost'.
# Modify if needed.
server_ip_address = 'localhost'


# Loop through the folder and process each RINEX file
for file in glob.glob(dir_rinex + '/*.Z'):
    file = os.path.abspath(file)
    print file
    rname = file[-10:]
    sname = file[-15:-10]
    dt_refdate = datetime.strptime(rname,'%j0.%yD.Z')
    refdate = dt_refdate.strftime('%Y-%m-%d %H:%M:%S')
    cmd = rqc + ' --mask_fil ' + file + ' --date_ref "' + refdate + '"'
    print cmd
    subprocess.check_call(cmd, shell=True)
    
    # this only works for 24h RINEX2 files!!!
    xmlpath = datetime.strftime(dt_refdate,'LOG/' + sname + '%y%j0.xml')
    if os.path.isfile(xmlpath):
        with open(xmlpath, 'r') as f:
            body = f.read()
    else:
        print 'Oh no!!!'
        os.exit(1)

    address = 'http://'+server_ip_address+':5000/gps/data/qcfile'

    r = requests.post(address, data=body)
    p_json = json.loads(r.text) # parse JSON string
    
    print "QCXML {}, Status code: {} Text:".format(
            xmlpath, r.status_code)
    print json.dumps(p_json, indent=4, sort_keys=True)
