import time
from functools import wraps
import requests

PROF_DATA = {}

org = 'http://localhost:5555'


def call_delete_station(station_long_marker):
    url = org + '/gps/station/'+station_long_marker

    try:
        res = requests.delete(url)
        print (res.text)
    except Exception as e:
        print("error: %s" % str(e))
        return

def call_site_log(json_data):
    url = org + '/gps/sitelog'
    head = {'Content-type': 'application/json'}

    try:
        res = requests.post(url, data=json_data, headers=head)
        print (res.text)

        res = requests.post(url, data=json_data, headers=head)
        print (res.text)

    except Exception as e:
        print("error: %s" % str(e))
        return

def main():
    with open('AVR200FRA.json', 'r') as f:
        data = f.read()
        for i in xrange(0, 10):
            print (i)
            call_delete_station('AVR200FRA')
            call_site_log(data)

if __name__ == '__main__':
    main()
