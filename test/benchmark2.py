import time
from functools import wraps
import requests
import inspect
from numpy import random

PROF_DATA = {}

def profile(fn):
    @wraps(fn)
    def with_profiling(*args, **kwargs):

        start_time = time.time()

        ret = fn(*args, **kwargs)

        elapsed_time = time.time() - start_time

        if fn.__name__ not in PROF_DATA:
            PROF_DATA[fn.__name__] = {'called_times': 0, 'max_time': 0, 'avg_time': 0}
        if elapsed_time > PROF_DATA[fn.__name__]['max_time']:
            PROF_DATA[fn.__name__]['max_time'] = elapsed_time
        PROF_DATA[fn.__name__]['avg_time'] = (PROF_DATA[fn.__name__]['avg_time'] * PROF_DATA[fn.__name__]['called_times'] + elapsed_time) / (PROF_DATA[fn.__name__]['called_times'] + 1)
        PROF_DATA[fn.__name__]['called_times'] += 1
        print (get_message(fn.__name__))
        return ret
    return with_profiling

def get_message(func_name):
     msg = "Function %s called %d times. Execution time max: %.3f, average: %.3f" \
                    % ( func_name,
                       PROF_DATA[func_name]['called_times'],
                       PROF_DATA[func_name]['max_time'],
                       PROF_DATA[func_name]['avg_time'])
     return msg

@profile
def test_func1():
    seconds = random.random_integers(1,4)
    time.sleep(seconds)


@profile
def test_func2():
    seconds = random.random_integers(1,3)
    time.sleep(seconds)

def main():
    for i in range(0, 10):
        test_func1()
        test_func2()

if __name__ == '__main__':
    main()