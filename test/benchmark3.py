import time
from functools import wraps
import requests
from numpy import random


PROF_DATA = {}

org = 'http://localhost:5555'


def profile(callback=None):

    def inner_function(func):

        @wraps(func)
        def with_profiling(*args, **kwargs):
            start_time = time.time()

            ret = func(*args, **kwargs)

            elapsed_time = time.time() - start_time

            if func.__name__ not in PROF_DATA:
                PROF_DATA[func.__name__] = {'called_times': 0, 'max_time': 0, 'avg_time': 0}
            if elapsed_time > PROF_DATA[func.__name__]['max_time']:
                PROF_DATA[func.__name__]['max_time'] = elapsed_time
            PROF_DATA[func.__name__]['avg_time'] = (PROF_DATA[func.__name__]['avg_time'] * PROF_DATA[func.__name__][
                'called_times'] + elapsed_time) / (PROF_DATA[func.__name__]['called_times'] + 1)
            PROF_DATA[func.__name__]['called_times'] += 1

            msg = "Function %s called %d times. Execution time last: %.3f,  max: %.3f, average: %.3f" \
                  % (func.__name__,
                     PROF_DATA[func.__name__]['called_times'],
                     elapsed_time,
                     PROF_DATA[func.__name__]['max_time'],
                     PROF_DATA[func.__name__]['avg_time'])

            if callback:
                callback(msg)
            return ret

        return with_profiling
    return inner_function

def print_benchmark(message):
       print (message)

@profile(print_benchmark)
def test_func1():
    seconds = random.random_integers(1,4)
    time.sleep(seconds)


@profile()
def test_func2():
    seconds = random.random_integers(1,3)
    time.sleep(seconds)

def main():
    for i in range(0, 10):
        test_func1()
        test_func2()

if __name__ == '__main__':
    main()